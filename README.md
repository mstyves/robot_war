# Schema de l'adapteur

https://store.curiousinventor.com/guides/PS2

# BOM

- Moteurs: https://www.aliexpress.com/item/32887612577.html
- Roues: https://www.aliexpress.com/item/4000410915199.html
- Arduino pro mini 3.3v: https://www.aliexpress.com/item/32672852945.html
- Controlleur moteur double en H: https://www.aliexpress.com/item/32896620313.html
- Controlleur RF Sony PS2: https://www.aliexpress.com/item/33020572558.html
- Frame de base: https://www.aliexpress.com/item/32955666490.html (voir format imprimé)
